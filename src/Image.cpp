#include "Image.h"
#include <cassert>
#include <time.h>
#include <iostream>


// Constructeur surchargé : initialise dimx et dimy (apr�s v�rification)
// puis alloue le tableau de pixel dans le tas (image noire)
Image::Image(unsigned int x, unsigned int y) : dimx(x), dimy(y), tab(nullptr) {
    if (dimx > 0 && dimy > 0) tab = new Pixel[dimx * dimy]();
    else {
        std::cerr << "Erreur : Dimensions invalides lors de la création de l'image." << std::endl;
        dimx = dimy = 0;
    }
}
// Destructeur : d'allocation de la mémoire du tableau de pixels
// et mise à jour des champs dimx et dimy = 0
Image::~Image() {
    if(tab != nullptr){
        delete[] tab;
        tab = nullptr;
    }
    dimx = dimy = 0;
}

// Récupère le pixel original de coordonnées (x,y) en vérifiant sa validité.
// La formule pour passer d'un tab 2D à un tab 1D est tab[y*dimx+x]
Pixel& Image::getPix(unsigned int x, unsigned int y) {
    if (x > dimx || y > dimy) {
        std::cerr << "Pixel innexistant" << std::endl;
        return tab[0];
    } 
    else return tab[y * dimx + x];
}

// Récupére le pixel original de coordonnées (x,y) en vérifiant sa validité.
//Fonction getPix(x, y : donnée entier) const-> Pixel(une copie du Pixel)
Pixel Image::getPix(unsigned int x, unsigned int y) const {
    if (x >= dimx || y >= dimy) {
        std::cerr << "Erreur : Coordonnées hors de la plage de l'image. Retourne un pixel par défaut." << std::endl;
        return Pixel(); 
    }
    else {
        return Pixel(tab[y * dimx + x]);
    }
}

// Modifie le pixel de coordonnées (x,y)
void Image::setPix(unsigned int x, unsigned int y, const Pixel& couleur) {
    if (dimx == 0 || dimy == 0) {
        std::cerr << "Impossible de modifier le pixel : l'image n'existe pas." << std::endl;
        return;
    }
    else if (x >= dimx || y >= dimy) {
        std::cerr << "Impossible de modifier le pixel : Coordonnées hors de la plage de l'image." << std::endl;
        return;
    }
    else {
        tab[y * dimx + x] = couleur;
    }
}

// Fonction pour obtenir un pointeur vers le tableau de pixels
const Pixel* Image::getPixelData() const {
    return tab;
}

unsigned int Image::getHauteur() const {
    return dimy;
}

// Fonction pour obtenir la largeur de l'image
unsigned int Image::getLargeur() const {
    return dimx;
}

// Dessine un rectangle plein de la couleur dans l'image
// (en utilisant setPix, indices en paramètre compris)

void Image::dessinerRectangle(unsigned int Xmin, unsigned int Ymin, unsigned int Xmax, unsigned int Ymax, const Pixel& couleur) {
    if (Xmax <= dimx || Ymax <= dimy) {
        for (unsigned int j = Ymin; j < Ymax; ++j) {
            for (unsigned int i = Xmin; i < Xmax; ++i) {
                setPix(i, j, couleur);
            }
        }
    }
    else {
        std::cerr << "Rectangle non valide" << std::endl;
        return;
    }

}

// Efface l'image en la remplissant de la couleur en paramètre
// (en appelant dessinerRectangle avec le bon rectangle)
void Image::effacer(const Pixel& couleur) {
    dessinerRectangle(0, 0, dimx, dimy, couleur);
}

// Effectue une série de tests vérifiant que toutes les fonctions fonctionnent et
// font bien ce qu'elles sont censées faire, ainsi que les données membres de
// l'objet sont conformes


void Image::sauver(const std::string & filename) const {
    std::ofstream fichier (filename.c_str());
    assert(fichier.is_open());
    fichier << "P3" << std::endl;
    fichier << dimx << " " << dimy << std::endl;
    fichier << "255" << std::endl;
    for(unsigned int y=0; y<dimy; ++y){
        for(unsigned int x=0; x<dimx; ++x) {
            Pixel pix = getPix(x,y);
            fichier << +pix.r << " " << +pix.g << " " << +pix.b << " ";
        }
        fichier << std::endl;
    }
    std::cout << "Sauvegarde de l'image " << filename << " ... OK\n";
    fichier.close();
}

void Image::ouvrir(const std::string & filename) {
    std::ifstream fichier (filename.c_str());
    assert(fichier.is_open());
	char r,g,b;
	std::string mot;
	dimx = dimy = 0;
	fichier >> mot >> dimx >> dimy >> mot;
	assert(dimx > 0 && dimy > 0);
	if (tab != NULL) delete [] tab;
	tab = new Pixel [dimx*dimy];
    for(unsigned int y=0; y<dimy; ++y)
        for(unsigned int x=0; x<dimx; ++x) {
            fichier >> r >> b >> g;
            getPix(x,y).r = r;
            getPix(x,y).g = g;
            getPix(x,y).b = b;
        }
    fichier.close();
    std::cout << "Lecture de l'image " << filename << " ... OK\n";
}

void Image::afficherConsole() const {
    std::cout << dimx << " " << dimy << std::endl;
    for(unsigned int y=0; y<dimy; ++y) {
        for(unsigned int x=0; x<dimx; ++x) {
            Pixel pix = getPix(x,y);
            std::cout << +pix.r << " " << +pix.g << " " << +pix.b << " ";
        }
        std::cout << std::endl;
    }
}

void Image::testRegression() {

    srand (time(NULL));

    //vérif image vide

    Image imgVide;

    assert(imgVide.dimx==0);
    assert(imgVide.dimy==0);
    assert(imgVide.tab==nullptr);

    //vérif img pas vide

    unsigned int randX = rand() % 100 + 1; //rand() % 100 + 1;
    unsigned int randY = rand() % 100 + 1; //rand() % 100 + 1;
    
    Image imgDim(randX, randY);

    assert(imgDim.dimx==randX);
    assert(imgDim.dimy==randY);

    if(imgDim.getLargeur()==randX);
    if(imgDim.getHauteur()==randY);
    if(imgDim.getPixelData()== imgDim.tab);

    for(unsigned int x=0; x<randX * randY; ++x) {
        assert(imgDim.tab[x].r==0);
        assert(imgDim.tab[x].g==0);
        assert(imgDim.tab[x].b==0);
    }
    
    //verif get et set 

    int randR = rand() % 255 ;
    int randG = rand() % 255 ;
    int randB = rand() % 255 ;

    for(unsigned int x=0; x<imgDim.dimx; ++x) {
        for(unsigned int y=0; y<imgDim.dimy; ++y) {
            //vérif set pix par référence
            Pixel& pix = imgDim.getPix(x,y);

            imgDim.setPix(x,y, Pixel(randR,randG,randB));

            assert(pix.r == randR);
            assert(pix.g == randG);
            assert(pix.b == randB);

            //vérif setPix par copie
            Pixel pixConst = imgDim.getPix(x,y);
            imgDim.setPix(x,y, Pixel(5,5,5));

            assert(int(pixConst.r) == randR);
            assert(int(pixConst.g) == randG);
            assert(int(pixConst.b) == randB);

            assert(pix.r == 5);
            assert(pix.g == 5);
            assert(pix.b == 5);
        }
    }
    unsigned int randXmax = rand() % (imgDim.dimx) +1;
    unsigned int randYmax = rand() % (imgDim.dimy) +1;
    unsigned int randXmin = rand() % randXmax;
    unsigned int randYmin = rand() % randYmax;

    randR = rand() % 255 ;
    randG = rand() % 255 ;
    randB = rand() % 255 ;
    imgDim.dessinerRectangle(randXmin , randYmin, randXmax , randYmax , Pixel(randR, randG , randB));

    for(unsigned int x=randXmin; x<randXmax; ++x) {
        for(unsigned int y=randYmin; y<randYmax; ++y) {
            Pixel& pix = imgDim.getPix(x,y);
            assert(pix.r == randR);
            assert(pix.g == randG);
            assert(pix.b == randB);
        }
    }

    imgDim.sauver("./data/imgSauvegarde.txt");
    std::ifstream fichier ("./data/imgSauvegarde.txt");

    assert(fichier.is_open());
    std::string str;
    fichier >> str;
    assert(str=="P3");
    fichier>> str;
    assert(str==std::to_string(imgDim.dimx));
    fichier >> str;
    assert(str==std::to_string(imgDim.dimy));
    fichier>>str;
    assert(str=="255");

    while (fichier >> str){
        for(unsigned int y=0; y<imgDim.dimy; ++y) {
            for(unsigned int x=0; x<imgDim.dimx; ++x) {
                Pixel& pix = imgDim.getPix(x,y);
                assert(str==std::to_string(pix.r));
                fichier >> str;
                assert(str==std::to_string(pix.g));
                fichier >> str;
                assert(str==std::to_string(pix.b));
                fichier >> str;
            }
        }
    }

    Image imgOpen ;

    imgOpen.ouvrir("./data/imgSauvegarde.txt");

    assert(imgDim.dimx == imgOpen.dimx);
    assert(imgDim.dimy == imgOpen.dimy);

    fichier.close();

    //vérif effacer()
    randR = rand() % 255 ;
    randG = rand() % 255 ;
    randB = rand() % 255 ;
    
    imgDim.effacer(Pixel(randR,randG,randB));

    for(unsigned int x=0; x<imgDim.dimx; ++x) {
        for(unsigned int y=0; y<imgDim.dimy; ++y) {
            Pixel& pix = imgDim.getPix(x,y);
            assert(pix.r == randR);
            assert(pix.g == randG);
            assert(pix.b == randB);
        }
    }

    
    imgDim.~Image();
    imgVide.~Image();
    assert(imgVide.tab==nullptr);
    assert(imgVide.dimx==0);
    assert(imgVide.dimy==0);
    
    const std::string filePath = "./data/imgSauvegarde.txt";

    std::ifstream file(filePath);
    if (file && std::ifstream(filePath)){
        std::remove("./data/imgSauvegarde.txt");
    }
}

