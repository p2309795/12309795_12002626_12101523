#ifndef PIXEL_H
#define PIXEL_H

/**
 * @struct Pixel
 * 
 * @brief Structure représentant un pixel RVB.
 *
 * La structure Pixel est utilisée pour représenter un pixel avec des composantes Rouge (r), Verte (g), et Bleue (b).
 * Chaque composante est représentée par un octet (unsigned char) pouvant prendre des valeurs de 0 à 255.
 *
 * @note Par défaut, un pixel est initialisé avec les composantes r, g, et b ayant des valeurs nulles (noir).
 * @note La structure peut également être initialisée avec des valeurs spécifiques pour chaque composante.
 * 
 * @see Image pour l'utilisation de la structure Pixel dans le contexte d'une image.
 */
struct Pixel {
	/**
     * @brief Composante rouge du pixel.
     */
	unsigned char r;
	/**
     * @brief Composante verte du pixel.
     */
	unsigned char g;
	/**
     * @brief Composante bleue du pixel.
     */
	unsigned char b;

     /**
     * @brief Constructeur par défaut.
     *
     * Initialise les composantes r, g, et b à 0, produisant un pixel noir.
     */
	Pixel() : r(0), g(0), b(0) {}

    /**
     * @brief Constructeur avec initialisation des composantes.
     *
     * Initialise les composantes r, g, et b avec les valeurs spécifiées.
     *
     * @param rr Valeur de la composante Rouge.
     * @param gg Valeur de la composante Verte.
     * @param bb Valeur de la composante Bleue.
     */
	Pixel(unsigned char rr, unsigned char gg, unsigned char bb) : r(rr), g(gg), b(bb) {}
};

#endif