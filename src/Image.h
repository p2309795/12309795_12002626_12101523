#ifndef IMAGE_H
#define IMAGE_H

#include "Pixel.h"
#include <fstream>
/**
 * @class Image
 *
 * @brief Classe représentant une image composée de pixels.
 *
 * Une classe pour illustrer l'utilisation de Doxygen pour générer de la documentation.
 */ 

class Image 
{
private:
    /**
     * @brief Tableau de pixels représentant l'image.
     */
    unsigned int dimx; /**< Dimensions de l'image (largeur et hauteur). */
    /**
     * @brief Tableau de pixels représentant l'image.
     */
    unsigned int dimy;
    /**
     * @brief Tableau de pixels représentant l'image.
     */
    Pixel* tab; /**< Tableau de pixels représentant l'image. */

public:
    /**
     * @brief Constructeur par défaut de la classe Image.
     *
     * Initialise les dimensions de l'image (dimx et dimy) à zéro et le tableau de pixels à nullptr.
     * Cela crée une image vide sans allocation de mémoire pour le tableau de pixels.
     *
     * @note Ce constructeur est utilisé pour créer une image vide sans dimensions ni pixels alloués.
     * @see Image(unsigned int x, unsigned int y) pour créer une image avec des dimensions spécifiées.
     */
    Image() : dimx(0), dimy(0), tab(nullptr) {}

    /**
     * @brief Constructeur surchargé de la classe Image.
     *
     * Initialise les dimensions de l'image (dimx et dimy) après vérification de leur validité.
     * Alloue dynamiquement le tableau de pixels dans le tas, initialisant l'image en noir.
     * En cas de dimensions invalides, affiche un message d'erreur sur la sortie d'erreur standard.
     *
     * @param x Largeur de l'image.
     * @param y Hauteur de l'image.
     *
     * @note Si les dimensions fournies sont invalides (x <= 0 ou y <= 0), le tableau de pixels n'est pas alloué,
     * et les dimensions de l'image sont mises à zéro.
     * @note Un tableau de pixels alloué dynamiquement est initialisé avec des pixels noirs.
     */
    Image(unsigned int x, unsigned int y);

    /**
     * @brief Destructeur de la classe Image.
     *
     * Libère la mémoire allouée dynamiquement pour le tableau de pixels.
     * Met à jour les dimensions de l'image (dimx et dimy) en les définissant à zéro.
     *
     * @note Si le tableau de pixels n'est pas alloué (pointeur nul), aucune action n'est effectuée.
     * @note Assurez-vous d'appeler ce destructeur pour libérer la mémoire allouée par un objet Image.
    */
    ~Image();

    /**
     * @brief Récupère le pixel d'origine aux coordonnées spécifiées en vérifiant leur validité.
     *
     * Cette fonction retourne une référence vers le pixel d'origine situé aux coordonnées (x, y) dans l'image.
     * La validité des coordonnées est vérifiée, et en cas de coordonnées invalides, un message d'erreur est affiché
     * sur la sortie d'erreur standard, et la référence du premier pixel est retournée.
     *
     * @param x Coordonnée horizontale du pixel.
     * @param y Coordonnée verticale du pixel.
     *
     * @return Pixel& Référence vers le pixel d'origine aux coordonnées spécifiées.
     *
     * @note Les coordonnées sont considérées valides si x < dimx et y < dimy.
     * @note Utilise la formule tab[y * dimx + x] pour accéder au pixel dans le tableau 1D.
     * @see Pixel pour la représentation d'un pixel.
     */
    Pixel& getPix(unsigned int x, unsigned int y);

    /**
     * @brief Récupère une copie du pixel d'origine aux coordonnées spécifiées en vérifiant leur validité.
     *
     * Cette fonction retourne une copie du pixel d'origine situé aux coordonnées (x, y) dans l'image.
     * La validité des coordonnées est vérifiée, et en cas de coordonnées invalides, un message d'erreur est affiché
     * sur la sortie d'erreur standard, et un pixel par défaut est retourné.
     *
     * @param x Coordonnée horizontale du pixel.
     * @param y Coordonnée verticale du pixel.
     *
     * @return Pixel Copie du pixel d'origine aux coordonnées spécifiées.
     *
     * @note Les coordonnées sont considérées valides si x < dimx et y < dimy.
     * @see Pixel pour la représentation d'un pixel.
     */
    Pixel getPix(unsigned int x, unsigned int y) const; // ->Pixel(une copie du Pixel)

    /**
     * @brief Modifie le pixel aux coordonnées spécifiées.
     *
     * Cette fonction modifie le pixel d'origine situé aux coordonnées (x, y) dans l'image en utilisant la couleur spécifiée.
     * La validité des coordonnées est vérifiée, et en cas de coordonnées invalides, un message d'erreur est affiché
     * sur la sortie d'erreur standard.
     *
     * @param x Coordonnée horizontale du pixel.
     * @param y Coordonnée verticale du pixel.
     * @param couleur Couleur à assigner au pixel.
     *
     * @note Les coordonnées sont considérées valides si x < dimx et y < dimy.
     * @note Si l'image n'existe pas (dimx ou dimy égal à 0), un message d'erreur est affiché et aucune modification n'est effectuée.
     * @see Pixel pour la représentation d'un pixel.
     */
    void setPix(unsigned int x, unsigned int y, const Pixel& couleur);

    /**
     * @brief Obtient un pointeur vers le tableau de pixels de l'image.
     *
     * Cette fonction retourne un pointeur constant vers le tableau de pixels de l'image.
     * Les pixels sont stockés dans un tableau unidimensionnel, et ce pointeur permet d'accéder à l'ensemble des pixels.
     *
     * @return const Pixel* Pointeur vers le tableau de pixels de l'image.
     *
     * @see Pixel pour la représentation d'un pixel.
     */
    const Pixel* getPixelData() const;

    /**
     * @brief Obtient la hauteur de l'image.
     *
     * Cette fonction retourne la hauteur de l'image, c'est-à-dire le nombre de lignes de pixels dans l'image.
     *
     * @return unsigned int Hauteur de l'image.
     */
    unsigned int getHauteur() const;

    /**
     * @brief Obtient la largeur de l'image.
     *
     * Cette fonction retourne la largeur de l'image, c'est-à-dire le nombre de colonnes de pixels dans l'image.
     *
     * @return unsigned int Largeur de l'image.
     */
    unsigned int getLargeur() const;

    /**
     * @brief Dessine un rectangle plein de la couleur spécifiée dans l'image.
     *
     * Cette fonction remplit un rectangle délimité par les coordonnées (Xmin, Ymin) et (Xmax, Ymax) inclusivement
     * avec la couleur spécifiée. Les coordonnées du rectangle sont vérifiées pour garantir leur validité
     * par rapport aux dimensions de l'image. En cas de coordonnées invalides, un message d'erreur est affiché
     * sur la sortie d'erreur standard, et aucune modification n'est effectuée.
     *
     * @param Xmin Coordonnée horizontale minimale du rectangle.
     * @param Ymin Coordonnée verticale minimale du rectangle.
     * @param Xmax Coordonnée horizontale maximale du rectangle.
     * @param Ymax Coordonnée verticale maximale du rectangle.
     * @param couleur Couleur à utiliser pour remplir le rectangle.
     *
     * @note Les coordonnées (Xmin, Ymin) et (Xmax, Ymax) doivent être incluses dans les dimensions de l'image.
     * @note Utilise la fonction setPix pour assigner la couleur à chaque pixel du rectangle.
     * @see Image::setPix pour la modification de la couleur d'un pixel.
     * @see Pixel pour la représentation d'un pixel.
     */
    void dessinerRectangle(unsigned int Xmin, unsigned int Ymin, unsigned int Xmax, unsigned int Ymax, const Pixel& couleur);

    /**
     * @brief Efface l'image en la remplissant avec la couleur spécifiée.
     *
     * Cette fonction efface l'intégralité de l'image en appelant la fonction dessinerRectangle avec les coordonnées
     * du rectangle correspondant aux dimensions de l'image et en utilisant la couleur spécifiée. Cela a pour effet
     * de remplir toute l'image avec la couleur spécifiée, effaçant ainsi son contenu précédent.
     *
     * @param couleur Couleur à utiliser pour remplir l'image.
     *
     * @note Utilise la fonction dessinerRectangle pour remplir l'image avec la couleur spécifiée.
     * @see dessinerRectangle pour remplir un rectangle avec une couleur spécifiée.
     * @see Image::dessinerRectangle
     */
    void effacer(const Pixel& couleur);

    /**
     * @brief Sauvegarde l'image dans un fichier au format PPM (Portable Pixmap).
     *
     * Cette fonction sauvegarde l'image dans un fichier au format PPM, spécifié par la norme P3.
     * Le fichier contient les dimensions de l'image, la valeur maximale de couleur (255), et les valeurs RGB
     * de chaque pixel. Les pixels sont séparés par des espaces et les lignes sont terminées par un retour à la ligne.
     *
     * @param filename Nom du fichier dans lequel l'image sera sauvegardée.
     *
     * @note Le format PPM P3 utilise des valeurs RGB de 0 à 255.
     * @note Les dimensions de l'image et la couleur des pixels sont extraites en utilisant la fonction getPix.
     * @see Image::getPix pour obtenir la couleur d'un pixel.
     * @see Pixel pour la représentation d'un pixel.
     */
    void sauver(const std::string & filename) const;

    /**
     * @brief Ouvre un fichier image au format PPM (Portable Pixmap) et initialise l'objet Image.
     *
     * Cette fonction ouvre un fichier image au format PPM (Portable Pixmap) spécifié par la norme P3.
     * Elle lit les dimensions de l'image, vérifie leur validité, alloue dynamiquement le tableau de pixels
     * dans le tas, puis lit les valeurs RGB de chaque pixel depuis le fichier pour les assigner à l'objet Image.
     *
     * @param filename Nom du fichier image à ouvrir.
     *
     * @note Le format PPM P3 utilise des valeurs RGB de 0 à 255.
     * @note Les dimensions de l'image sont extraites depuis le fichier, et le tableau de pixels est alloué dynamiquement.
     * @note En cas de dimensions invalides ou d'échec d'allocation mémoire, un message d'erreur est affiché et aucune modification n'est effectuée.
     * @see Pixel pour la représentation d'un pixel.
     */
    void ouvrir(const std::string & filename);

    /**
     * @brief Affiche les valeurs RGB de chaque pixel dans la console.
     *
     * Cette fonction affiche les valeurs RGB de chaque pixel de l'image dans la console.
     * Chaque ligne représente une ligne de pixels, et les valeurs RGB sont séparées par des espaces.
     *
     * @note Utilise la fonction getPix pour obtenir la couleur de chaque pixel.
     * @see Image::getPix pour obtenir la couleur d'un pixel.
     * @see Pixel pour la représentation d'un pixel.
     */
    void afficherConsole() const;

    /**
     * @brief Fonction statique de test de régression pour la classe Image.
     *
     * Cette fonction statique sert de point d'entrée pour les tests de régression de la classe Image.
     * Elle permet de tester différentes fonctionnalités de la classe Image afin de s'assurer de son bon fonctionnement.
     * Les tests peuvent inclure la création d'images, la modification de pixels, l'affichage, la sauvegarde, la lecture de fichiers, etc.
     *
     * @note Les tests de régression sont utilisés pour vérifier que les modifications apportées à la classe Image n'ont pas introduit de nouveaux bugs
     * et que les fonctionnalités existantes continuent de fonctionner correctement.
     * @see Image pour la classe que la fonction teste.
     */
    static void testRegression();
};

#endif
