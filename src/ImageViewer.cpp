
#include <iostream>
#include "ImageViewer.h"

ImageViewer::ImageViewer() {
    // Initialisation SDL
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        std::cerr << "Erreur lors de l'initialisation de SDL : " << SDL_GetError() << std::endl;
    }

    // Création de la fenêtre
    m_window = SDL_CreateWindow("Image Viewer", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 200, 200, SDL_WINDOW_SHOWN);
    if (m_window == nullptr) {
        std::cerr << "Erreur lors de la création de la fenêtre : " << SDL_GetError() << std::endl;
        SDL_Quit();
    }

    // Création du renderer
    m_renderer = SDL_CreateRenderer(m_window, -1, SDL_RENDERER_ACCELERATED);
    if (m_renderer == nullptr) {
        std::cerr << "Erreur lors de la création du renderer : " << SDL_GetError() << std::endl;
        SDL_DestroyWindow(m_window);
        SDL_Quit();
    }
}

ImageViewer::~ImageViewer() {
    SDL_DestroyRenderer(m_renderer);
    SDL_DestroyWindow(m_window);
    SDL_Quit();
}

void ImageViewer::afficher(const Image& im) {
    SDL_Event event;
    bool quit = false;
    double zoom = 1.0;

    while (!quit) {
        while (SDL_PollEvent(&event) != 0) {
            if (event.type == SDL_QUIT || (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_SPACE)) {
                quit = true;
            }
            else if (event.type == SDL_KEYDOWN) {
                if (event.key.keysym.sym == SDLK_t) {
                    zoom *= 1.1;
                }
                else if (event.key.keysym.sym == SDLK_g) {
                    zoom /= 1.1;
                }
            }
        }

        SDL_RenderClear(m_renderer);

        // Remplir le fond de gris clair
        SDL_SetRenderDrawColor(m_renderer, 211, 211, 211, 255);
        SDL_RenderFillRect(m_renderer, nullptr);

        // Convertir le tableau de pixels en SDL_Surface
        SDL_Surface* surface = SDL_CreateRGBSurfaceFrom(

            /*
            
            im.getPixelData() renvoie un pointeur constant vers les 
            données du tableau de pixels de l'objet Image.

            static_cast<const void*>(im.getPixelData()) effectue une conversion 
            de type statique pour changer le type du pointeur de const Pixel* à const void*. 
            Nécessaire car SDL_CreateRGBSurfaceFrom prend un pointeur void*.

            const_cast<void*>(static_cast<const void*>(im.getPixelData())) pour retirer la 
            qualification de constance du pointeur. Il est important de noter que cela est
            fait avec précaution, car cela pourrait entraîner des problèmes si les données 
            sous-jacentes étaient réellement modifiables.
            
            
            */
            const_cast<void*>(static_cast<const void*>(im.getPixelData())), 
            im.getLargeur(),
            im.getHauteur(),
            24, // Profondeur de couleur en bits
            im.getLargeur() * sizeof(Pixel),
            0x0000FF, // Masque pour la composante bleue
            0x00FF00, // Masque pour la composante verte
            0xFF0000, // Masque pour la composante rouge
            0
        );

        // Créer la texture à partir de la surface
        SDL_Texture* texture = SDL_CreateTextureFromSurface(m_renderer, surface);

        // Dessiner la texture sur le renderer avec le zoom
        SDL_Rect destRect = { 0, 0, static_cast<int>(im.getLargeur() * zoom), static_cast<int>(im.getHauteur() * zoom) };
        SDL_RenderCopy(m_renderer, texture, nullptr, &destRect);

        // Libérer les ressources
        SDL_FreeSurface(surface);
        SDL_DestroyTexture(texture);

        SDL_RenderPresent(m_renderer);
    }
}
