#ifndef IMAGEVIEWER_H   
#define IMAGEVIEWER_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "Image.h"

/**
 * @class ImageViewer
 * 
 * @brief Classe pour visualiser une image à l'aide de la bibliothèque SDL.
 *
 * La classe ImageViewer permet de créer une fenêtre SDL, d'afficher une image à l'intérieur de cette fenêtre
 * et de gérer les événements liés à la fermeture de la fenêtre.
 */
class ImageViewer
{
private:
    /**
     * @brief Pointeur vers la fenêtre SDL.
     */
    SDL_Window * m_window;

    /**
     * @brief Pointeur vers le renderer SDL.
     */
    SDL_Renderer * m_renderer;

public: 
    /**
     * @brief Constructeur de la classe ImageViewer.
     *
     * Initialise SDL, crée une fenêtre avec le titre "Image Viewer" et un renderer accéléré.
     * En cas d'échec lors de l'initialisation de SDL, de la création de la fenêtre ou du renderer,
     * des messages d'erreur sont affichés sur la sortie d'erreur standard, et les ressources sont libérées.
     * 
     * @note Cette fonction doit être appelée pour instancier un objet ImageViewer.
     */
    ImageViewer();

    /**
     * @brief Destructeur de la classe ImageViewer.
     * 
     * Libère les ressources allouées par l'objet ImageViewer, y compris le renderer et la fenêtre SDL.
     * Finalise l'initialisation de SDL en appelant SDL_Quit().
     * 
     * @note Cette fonction est automatiquement appelée lorsque l'objet ImageViewer est détruit.
     * @note Assurez-vous que toutes les instances de ImageViewer sont correctement détruites pour éviter des fuites de mémoire.
     */
    ~ImageViewer();

    /**
     * @brief Affiche une image dans une fenêtre SDL avec possibilité de zoom.
     *
     * Cette fonction crée une boucle d'affichage SDL qui permet d'afficher une image donnée.
     * L'utilisateur peut quitter la fenêtre en appuyant sur la touche 'Esc' ou en fermant la fenêtre.
     * De plus, la touche 't' permet d'augmenter le zoom de l'image, et la touche 'g' permet de réduire le zoom.
     *
     * @param im L'image à afficher.
     *
     * @note La fonction utilise SDL pour créer et gérer la fenêtre, le renderer, et les événements.
     * @note La boucle d'affichage continue jusqu'à ce que l'utilisateur quitte en appuyant sur 'Espace' ou en fermant la fenêtre.
     * @note La touche 't' permet d'augmenter le zoom de l'image de 10% à chaque pression.
     * @note La touche 'g' permet de réduire le zoom de l'image de 10% à chaque pression.
     * @note Cette fonction peut être utilisée pour visualiser une image avec la possibilité d'ajuster le zoom.
     * @note Assurez-vous que la fenêtre SDL a été correctement initialisée avant d'appeler cette fonction.
     * @note Veillez à respecter les règles de gestion de la mémoire et à libérer les ressources correctement après utilisation.
     */
    void afficher (const Image &im);

};

#endif