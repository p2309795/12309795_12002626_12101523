all: bin/test bin/affichage bin/exemple

bin/test: ./obj/mainTest.o ./obj/Image.o
	g++ -g ./obj/mainTest.o ./obj/Image.o -o bin/test

bin/exemple: ./obj/mainExemple.o ./obj/Image.o
	g++ -g ./obj/mainExemple.o ./obj/Image.o -o bin/exemple

bin/affichage : ./obj/mainAffichage.o ./obj/ImageViewer.o ./obj/Image.o 
	g++ -g ./src/mainAffichage.cpp ./obj/ImageViewer.o ./obj/Image.o -o bin/affichage -lSDL2 -lSDL2_image

doc: doc/doxyfile
	doxygen doc/doxyfile

obj/Image.o: ./src/Image.cpp ./src/Image.h ./src/Pixel.h
	g++ -g -Wall -c ./src/Image.cpp -o obj/Image.o

obj/mainExemple.o: ./src/mainExemple.cpp ./src/Image.h 
	g++ -g -Wall -c ./src/mainExemple.cpp -o obj/mainExemple.o

obj/mainTest.o: ./src/mainTest.cpp ./src/Image.h 
	g++ -g -Wall -c ./src/mainTest.cpp -o obj/mainTest.o

obj/ImageViewer.o : ./src/ImageViewer.cpp ./src/ImageViewer.h ./src/Image.h
	g++ -g -Wall -c ./src/ImageViewer.cpp -I~/usr/include -o obj/ImageViewer.o

obj/mainAffichage.o : ./src/mainAffichage.cpp ./src/ImageViewer.h
	g++ -g -Wall -c ./src/mainAffichage.cpp -o obj/mainAffichage.o

clean:
	rm obj/*.o bin/affichage bin/exemple bin/test