Générateur d'images à partir de pixels avec SDL

Ce projet vous permet de créer des images de couleur à partir de pixels créés individuellement et de les afficher à l'aide de la bibliothèque SDL.


Installation

Assurez-vous d'avoir SDL installé sur votre système. Vous pouvez le faire en utilisant votre gestionnaire de paquets ou en le téléchargeant depuis le site officiel de SDL.


Clonez ce dépôt sur votre machine locale en utilisant la commande :

git clone https://forge.univ-lyon1.fr/p2309795/12309795_12002626_12101523.git


L'archive se compose de 5 dossiers :

"bin" qui comporte les différents exécutables.
"data" qui comporte les deux images créées après l'exécution de l'exécutable exemple.
"doc" qui comporte la documentation doxyfile et l'HTML qui permet d'y avoir accès.
"obj" qui comporte les différents fichiers objets.
"src" qui comporte les fichiers .h et .cpp utilisés.


Pour compiler le projet, utilisez la commande : make.


Pour exécuter les différents exécutables :

./bin/affichage 

Permet d'afficher une image à l'écran.


./bin/exemple

Permet de générer un fichier .ppm avec la représentation en pixel d'une image prédéfinie.

./bin/test

Permet d'exécuter la fonction testRegression() qui vérifie que toutes les fonctions de la classe image soient bien implémentées.


Contact

Delort Alexandre 12309795, Retter Gaspard 12101523, Vallot Tanguy 12002626